import glob
import json
import requests
import sys
import os




class FlockNotification:

    # report_url = f'https://zeta-jenkins-reports.s3.ap-south-1.amazonaws.com/chaos/reportdump/{sys.argv[2]}/{sys.argv[3]}/'

    report_Dump_link=f'https://zeta-jenkins-reports.s3.ap-south-1.amazonaws.com/chaos/reportdump/{sys.argv[2]}/{sys.argv[3]}/'

    Json_dump_link =f'https://zeta-jenkins-reports.s3.ap-south-1.amazonaws.com/chaos/jsondump/{sys.argv[2]}/{sys.argv[3]}/'

    Final_Report_link=f'https://zeta-jenkins-reports.s3.ap-south-1.amazonaws.com/chaos/finalreport/{sys.argv[2]}-{sys.argv[3]}-finalreport.html'

    Build_url =f'https://build.internal.olympus-world.zetaapps.in/job/chaos_engg_experiment/{sys.argv[2]}/console'


    def __init__(self):
        self.failed=0
        self.passed=0
        self.failed_services=""
        self.passed_services=""
        self.total=0
        self.data={}



    def send_notification(self):
        print(sys.path.append(os.getcwd()))
        files=[data_file for data_file in sorted(glob.glob(f"./chaos-reports/jsondump/{sys.argv[2]}/{sys.argv[3]}/*.json"))]
        print(files)
        self.total=len(files)
        for json_file in files:
            with open(json_file,'r') as report_object:
                report = json.load(report_object)
                if report['status']=='failed':

                    self.failed_services += " <p>" + str(report['experiment']['method'][0]['provider']['arguments']['label_selector'].split('=')[1]) + "</p> <br/>"
                    self.failed+=1
                if report['status']=='completed':
                    self.passed+=1
                    self.passed_services += " <p>" + str(report['experiment']['method'][0]['provider']['arguments']['label_selector'].split('=')[1]) + "</p> <br/>"


        print(self.failed)
        print(self.passed)




        # Report<final report>  | Report Dir < Report Dump link> | Json Dir <Json dump link>
       # {'<flockml><a href='}{self.report_Dump_link}{'>Report Dir</a>'}{'| '} \
           #     {'<flockml><a href='}{self.Json_dump_link}{'>Json Dir</a>'}{'| '} \

        flockNotificationRequestBody = f'''{'<flockml><a href='}{self.Final_Report_link}{'>Report</a>'}{'| '} \
        {'<flockml><a href='}{self.Build_url}{'>Build_url</a>'}{'| '} \
            {'<br/><b>Experiment: </b>'}<b>{sys.argv[3]}</b> \
            {'<br/> <b>Total Services : </b>'}{self.total}{'<br/>'}{'<br/> <b>Failed : </b>'}{self.failed}{'<br/>'} \
               {'<br/> <b style="color : red;text-align:center;"> FAILED SERVICES :</b><br/>'} {self.failed_services}{'<br/>'} \
                {'<br/> <b style="color : green;text-align:center;"> PASSED SERVICES :</b><br/>'} {self.passed_services}{'<br/>'}    {'</flockml>'}'''
        self.data['flockml'] = flockNotificationRequestBody


        env = sys.argv[1]

        if env.lower() == 'beta':
            url = 'https://api.flock.com/hooks/sendMessage/fc2f23fc-7367-4c4b-8764-9e2937cc4f05'
        elif env.lower() == 'stage':
            url = 'https://api.flock.com/hooks/sendMessage/fc2f23fc-7367-4c4b-8764-9e2937cc4f05'
        else:
            raise("Incorrect arguments")

        if self.passed > 0 or self.failed >0:
            requests.post(url, headers={"Content-Type": "application/json"}, data=json.dumps(self.data))


FlockNotificationObj = FlockNotification()
FlockNotificationObj.send_notification()


#python3 FlockNotification.py beta buildnumber test_experiment