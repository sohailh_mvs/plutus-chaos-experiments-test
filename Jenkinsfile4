def projectName = "${params.projectName}"
def branchName = "*/master"



pipeline {
    options {
        buildDiscarder logRotator(numToKeepStr: '30')
        disableConcurrentBuilds()
        timeout(time: 120, unit: 'MINUTES')
    }

    agent {
        kubernetes {
            label "${projectName}-build-agent"
            yaml """
              apiVersion: v1
              kind: Pod
              metadata:
                annotations:
                  vault.security.banzaicloud.io/vault-ct-secrets-mount-path: "/vault/secrets/"
                  vault.security.banzaicloud.io/vault-ct-configmap: "vault-ct-docker-report-ow-world"
                  vault.security.banzaicloud.io/vault-addr: "https://vault.default:8200"
                  vault.security.banzaicloud.io/vault-role: "cluster-service"
                  vault.security.banzaicloud.io/vault-skip-verify: "true"
                  vault.security.banzaicloud.io/vault-path: "kubernetes/"
              spec:
                volumes:
                - name: config-vol
                  configMap:
                    name: docker-config
                containers:
                - name: helm
                  image: 813361731051.dkr.ecr.ap-south-1.amazonaws.com/dockerhub:helm-kubesec-python-v2
                  command: ["sleep"]
                  args: ["36000"]
                  tty: true
                  volumeMounts:
                    - name: config-vol
                      mountPath: ~/.docker
                  resources:
                    requests:
                      cpu: 500m
                      memory: 1Gi
                    limits:
                      cpu: 1
                      memory: 2Gi
                 """
        }
    }

    stages {
        stage('Code checkout') {
            steps {
                container('helm') {
                    checkout([$class: 'GitSCM',
                        branches: [[name: "${branchName}"]],
                        userRemoteConfigs: [[credentialsId: "bitbucket", url: "git@bitbucket.org:sohailh_mvs/plutus-chaos-experiments-test.git"]]])
                }
            }
        }

        stage('Setting up kubectl') {
                    steps {
                        container('helm') {
                            sh"""#!/bin/bash
                                # Install kubectl.
                                wget  https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl
                                chmod +x ./kubectl
                                mv ./kubectl /usr/local/bin/kubectl
                                kubectl version --client
                                """
                        }
                    }
                }
        stage('Setting up Helm') {
            steps {
                container('helm') {
                    sh"""#!/bin/bash
                        # Install helm
                        wget https://get.helm.sh/helm-v3.4.1-linux-amd64.tar.gz
                        tar xvf helm-v3.4.1-linux-amd64.tar.gz
                        mv linux-amd64/helm /usr/local/bin
                        rm helm-v3.4.1-linux-amd64.tar.gz
                        rm -rf linux-amd64
                        helm version
                        """
                }
            }
        }
        stage('cluster access') {
            steps {
                container('helm') {
                    sh"""#!/bin/bash


echo "-----BEGIN CERTIFICATE-----
MIIFRzCCBC+gAwIBAgISBGNwWMF5BTSYqMYmYitjaXMpMA0GCSqGSIb3DQEBCwUA
MDIxCzAJBgNVBAYTAlVTMRYwFAYDVQQKEw1MZXQncyBFbmNyeXB0MQswCQYDVQQD
EwJSMzAeFw0yMjA0MTcxMzE1NDdaFw0yMjA3MTYxMzE1NDZaMCsxKTAnBgNVBAMM
ICouaW50ZXJuYWwuaGRmYy1iZXRhLnpldGFhcHBzLmluMIIBIjANBgkqhkiG9w0B
AQEFAAOCAQ8AMIIBCgKCAQEAr7htiNH08sDa03PjnK1Jj01CxyZLqe0234So3Q8m
SJ2KYYmNaenAF7sO2ewqz8XL1IzXb+cr1TYtAGlsqr17xbs26iviPgBo4M+OHJOl
5vdHqAzi2Zf2GCk7KEwc8DGR+uZ1r+sLBSrFvnZW/Rmp9yidWYaI0ESC20X3r+vV
tsa4pW3heHTgxJc6aJSAros5VJDc4WCyLNu1NGK6sjVIa6U+tgmXKrTxO/+5KXxl
OwoFJ0knxFeyVY7doiN685B4QdmvnOHebvrLBZtHjM8BWiScg00OuptlC8eXSQli
bDcZCQthPAZtlg5fsqd3MyoGb60LZO0HzaOaHc9yIuaTLwIDAQABo4ICXDCCAlgw
DgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAM
BgNVHRMBAf8EAjAAMB0GA1UdDgQWBBQ8BRupiVzEkdaRNTexle7K7JwNTTAfBgNV
HSMEGDAWgBQULrMXt1hWy65QCUDmH6+dixTCxjBVBggrBgEFBQcBAQRJMEcwIQYI
KwYBBQUHMAGGFWh0dHA6Ly9yMy5vLmxlbmNyLm9yZzAiBggrBgEFBQcwAoYWaHR0
cDovL3IzLmkubGVuY3Iub3JnLzArBgNVHREEJDAigiAqLmludGVybmFsLmhkZmMt
YmV0YS56ZXRhYXBwcy5pbjBMBgNVHSAERTBDMAgGBmeBDAECATA3BgsrBgEEAYLf
EwEBATAoMCYGCCsGAQUFBwIBFhpodHRwOi8vY3BzLmxldHNlbmNyeXB0Lm9yZzCC
AQUGCisGAQQB1nkCBAIEgfYEgfMA8QB3AN+lXqtogk8fbK3uuF9OPlrqzaISpGpe
jjsSwCBEXCpzAAABgDfhNqgAAAQDAEgwRgIhAK1v2g84+qfBeMmMeMF82WHelFuF
3+zAK4oaBt6xulvUAiEAp4aYd2Vg1c/qq7zbKErc36TpwNn/ubmvHqXArsCJICIA
dgBGpVXrdfqRIDC1oolp9PN9ESxBdL79SbiFq/L8cP5tRwAAAYA34Tc9AAAEAwBH
MEUCIQDRN58C4R6e4bi8m/mYMBvDDtQcHyQAakiirCx2B04bQAIgOP1ln6zzA9Ug
VaP94aamJ8CaTTd8d4Wmmafr65gYzKkwDQYJKoZIhvcNAQELBQADggEBAC/nwiSY
Ao4uDpim1zPQORCexetm9nrLmVcf6/rbyorBAffcjLKpMjWZ8ETkWrDStmC/nlUr
pjGZV0HRE/dRJldI5Hjyy5wmuRFfyOc6MlONwWOCO7rBpwU9Oz3Nn0SFzQpNfdX7
bMcz4NyLK4EgqqlhWAJV9EK2exM0mDlH5BOacNGrEn3iu01Vfakuadfzdg9PQhXp
+lEVdKm7rO5LthaotEyfjlloYr61+ylhevyVPVvFCF0MAfYrzLebwyXRNkRZJH+o
3OHfE1qbr1wCXRMhEp8LkS1DrTIY+ZG+mtvkCPpzVClpO7TZNDLHRAdxN38hQBhW
o5Brp6kxtC9ONM4=
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFFjCCAv6gAwIBAgIRAJErCErPDBinU/bWLiWnX1owDQYJKoZIhvcNAQELBQAw
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwHhcNMjAwOTA0MDAwMDAw
WhcNMjUwOTE1MTYwMDAwWjAyMQswCQYDVQQGEwJVUzEWMBQGA1UEChMNTGV0J3Mg
RW5jcnlwdDELMAkGA1UEAxMCUjMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
AoIBAQC7AhUozPaglNMPEuyNVZLD+ILxmaZ6QoinXSaqtSu5xUyxr45r+XXIo9cP
R5QUVTVXjJ6oojkZ9YI8QqlObvU7wy7bjcCwXPNZOOftz2nwWgsbvsCUJCWH+jdx
sxPnHKzhm+/b5DtFUkWWqcFTzjTIUu61ru2P3mBw4qVUq7ZtDpelQDRrK9O8Zutm
NHz6a4uPVymZ+DAXXbpyb/uBxa3Shlg9F8fnCbvxK/eG3MHacV3URuPMrSXBiLxg
Z3Vms/EY96Jc5lP/Ooi2R6X/ExjqmAl3P51T+c8B5fWmcBcUr2Ok/5mzk53cU6cG
/kiFHaFpriV1uxPMUgP17VGhi9sVAgMBAAGjggEIMIIBBDAOBgNVHQ8BAf8EBAMC
AYYwHQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMBMBIGA1UdEwEB/wQIMAYB
Af8CAQAwHQYDVR0OBBYEFBQusxe3WFbLrlAJQOYfr52LFMLGMB8GA1UdIwQYMBaA
FHm0WeZ7tuXkAXOACIjIGlj26ZtuMDIGCCsGAQUFBwEBBCYwJDAiBggrBgEFBQcw
AoYWaHR0cDovL3gxLmkubGVuY3Iub3JnLzAnBgNVHR8EIDAeMBygGqAYhhZodHRw
Oi8veDEuYy5sZW5jci5vcmcvMCIGA1UdIAQbMBkwCAYGZ4EMAQIBMA0GCysGAQQB
gt8TAQEBMA0GCSqGSIb3DQEBCwUAA4ICAQCFyk5HPqP3hUSFvNVneLKYY611TR6W
PTNlclQtgaDqw+34IL9fzLdwALduO/ZelN7kIJ+m74uyA+eitRY8kc607TkC53wl
ikfmZW4/RvTZ8M6UK+5UzhK8jCdLuMGYL6KvzXGRSgi3yLgjewQtCPkIVz6D2QQz
CkcheAmCJ8MqyJu5zlzyZMjAvnnAT45tRAxekrsu94sQ4egdRCnbWSDtY7kh+BIm
lJNXoB1lBMEKIq4QDUOXoRgffuDghje1WrG9ML+Hbisq/yFOGwXD9RiX8F6sw6W4
avAuvDszue5L3sz85K+EC4Y/wFVDNvZo4TYXao6Z0f+lQKc0t8DQYzk1OXVu8rp2
yJMC6alLbBfODALZvYH7n7do1AZls4I9d1P4jnkDrQoxB3UqQ9hVl3LEKQ73xF1O
yK5GhDDX8oVfGKF5u+decIsH4YaTw7mP3GFxJSqv3+0lUFJoi5Lc5da149p90Ids
hCExroL1+7mryIkXPeFM5TgO9r0rvZaBFOvV2z0gp35Z0+L4WPlbuEjN/lxPFin+
HlUjr8gRsI3qfJOQFy/9rKIJR0Y/8Omwt/8oTWgy1mdeHmmjk7j1nYsvC9JSQ6Zv
MldlTTKB3zhThV1+XWYp6rjd5JW1zbVWEkLNxE7GJThEUG3szgBVGP7pSWTUTsqX
nLRbwHOoq7hHwg==
-----END CERTIFICATE-----
-----BEGIN CERTIFICATE-----
MIIFYDCCBEigAwIBAgIQQAF3ITfU6UK47naqPGQKtzANBgkqhkiG9w0BAQsFADA/
MSQwIgYDVQQKExtEaWdpdGFsIFNpZ25hdHVyZSBUcnVzdCBDby4xFzAVBgNVBAMT
DkRTVCBSb290IENBIFgzMB4XDTIxMDEyMDE5MTQwM1oXDTI0MDkzMDE4MTQwM1ow
TzELMAkGA1UEBhMCVVMxKTAnBgNVBAoTIEludGVybmV0IFNlY3VyaXR5IFJlc2Vh
cmNoIEdyb3VwMRUwEwYDVQQDEwxJU1JHIFJvb3QgWDEwggIiMA0GCSqGSIb3DQEB
AQUAA4ICDwAwggIKAoICAQCt6CRz9BQ385ueK1coHIe+3LffOJCMbjzmV6B493XC
ov71am72AE8o295ohmxEk7axY/0UEmu/H9LqMZshftEzPLpI9d1537O4/xLxIZpL
wYqGcWlKZmZsj348cL+tKSIG8+TA5oCu4kuPt5l+lAOf00eXfJlII1PoOK5PCm+D
LtFJV4yAdLbaL9A4jXsDcCEbdfIwPPqPrt3aY6vrFk/CjhFLfs8L6P+1dy70sntK
4EwSJQxwjQMpoOFTJOwT2e4ZvxCzSow/iaNhUd6shweU9GNx7C7ib1uYgeGJXDR5
bHbvO5BieebbpJovJsXQEOEO3tkQjhb7t/eo98flAgeYjzYIlefiN5YNNnWe+w5y
sR2bvAP5SQXYgd0FtCrWQemsAXaVCg/Y39W9Eh81LygXbNKYwagJZHduRze6zqxZ
Xmidf3LWicUGQSk+WT7dJvUkyRGnWqNMQB9GoZm1pzpRboY7nn1ypxIFeFntPlF4
FQsDj43QLwWyPntKHEtzBRL8xurgUBN8Q5N0s8p0544fAQjQMNRbcTa0B7rBMDBc
SLeCO5imfWCKoqMpgsy6vYMEG6KDA0Gh1gXxG8K28Kh8hjtGqEgqiNx2mna/H2ql
PRmP6zjzZN7IKw0KKP/32+IVQtQi0Cdd4Xn+GOdwiK1O5tmLOsbdJ1Fu/7xk9TND
TwIDAQABo4IBRjCCAUIwDwYDVR0TAQH/BAUwAwEB/zAOBgNVHQ8BAf8EBAMCAQYw
SwYIKwYBBQUHAQEEPzA9MDsGCCsGAQUFBzAChi9odHRwOi8vYXBwcy5pZGVudHJ1
c3QuY29tL3Jvb3RzL2RzdHJvb3RjYXgzLnA3YzAfBgNVHSMEGDAWgBTEp7Gkeyxx
+tvhS5B1/8QVYIWJEDBUBgNVHSAETTBLMAgGBmeBDAECATA/BgsrBgEEAYLfEwEB
ATAwMC4GCCsGAQUFBwIBFiJodHRwOi8vY3BzLnJvb3QteDEubGV0c2VuY3J5cHQu
b3JnMDwGA1UdHwQ1MDMwMaAvoC2GK2h0dHA6Ly9jcmwuaWRlbnRydXN0LmNvbS9E
U1RST09UQ0FYM0NSTC5jcmwwHQYDVR0OBBYEFHm0WeZ7tuXkAXOACIjIGlj26Ztu
MA0GCSqGSIb3DQEBCwUAA4IBAQAKcwBslm7/DlLQrt2M51oGrS+o44+/yQoDFVDC
5WxCu2+b9LRPwkSICHXM6webFGJueN7sJ7o5XPWioW5WlHAQU7G75K/QosMrAdSW
9MUgNTP52GE24HGNtLi1qoJFlcDyqSMo59ahy2cI2qBDLKobkx/J3vWraV0T9VuG
WCLKTVXkcGdtwlfFRjlBz4pYg1htmf5X6DYO8A4jqv2Il9DjXA6USbW1FzXSLr9O
he8Y4IWS6wY7bCkjCWDcRQJMEhg76fsO3txE+FiYruq9RUWhiF1myv4Q6W+CyBFC
Dfvp7OOGAN6dEOM4+qR9sdjoSYKEBpsr6GtPAQw4dy753ec5
-----END CERTIFICATE-----
"  > ca-aws-hdfc-beta-mumbai.pem
kubectl config set-cluster aws-hdfc-beta-mumbai --server=https://api-server-proxy.internal.hdfc-beta.zetaapps.in --certificate-authority=ca-aws-hdfc-beta-mumbai.pem --embed-certs
kubectl config set-credentials ChUxMTI5MDc0NDI3NjIzMTgzNjE1MDESBG9pZGM@aws-hdfc-beta-mumbai  --auth-provider=oidc  --auth-provider-arg='idp-issuer-url=https://dex.internal.hdfc-beta.zetaapps.in' --auth-provider-arg='client-id=gangway'  --auth-provider-arg='client-secret=ZXhhbXBsZS1hcHAtc2VjcmV0'  --auth-provider-arg='refresh-token=Chluem1kam5obmFuaWNvbDJjdWlxeXgzdm43Ehl0ejYzcm9td2tmZ24ybWNjYmFpbm83dG5i' --auth-provider-arg='id-token=eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ2M2FhOTVlNzBkZjk0YjIzN2ZkYmU0MTYzNDViNzQwMjBiNzI1MGYifQ.eyJpc3MiOiJodHRwczovL2RleC5pbnRlcm5hbC5oZGZjLWJldGEuemV0YWFwcHMuaW4iLCJzdWIiOiJDaFV4TVRJNU1EYzBOREkzTmpJek1UZ3pOakUxTURFU0JHOXBaR00iLCJhdWQiOiJnYW5nd2F5IiwiZXhwIjoxNjUxMDk1NTg1LCJpYXQiOjE2NTEwMDkxODUsImF0X2hhc2giOiJINHdSS2ZWLWpfeWtQa1RJRXRMVndnIiwiY19oYXNoIjoiUFJjQ2FWWmNLcDRBQlJTTlRVV1lqUSIsImVtYWlsIjoic29oYWlsaC5tdnNAZXh0LnpldGEudGVjaCIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJncm91cHMiOlsiYWxsQGV4dC56ZXRhLnRlY2giLCJsZWFkLWs4c0Bub3ZhLnRlY2giXSwibmFtZSI6IlNvaGFpbCBNaXIgSHVzc2FpbmkifQ.O2wVz9EpA_XIL0wM-zJTuaAJ9k6tVBhnOL60ZJhl1MJP6D1jirdYRMoIpsGS7O-yb4i8PhrnTvbN4Jl_7-Cn-9PMlNsQtDpAyOiUspD1sQLozSLYnWoMuJ7xTQflNG-oU12BE-910__1FQ-oUvMiJLiPqSTvR70P3VwXjR4jfmZ0S4nvG6P4oUZbFwgJ4W5X3s70PWDypuikRTnCPm-0pxIfqQ0vpz6Ny_lwppHTtxaBVgmPlHKnVVE8hhQiJSY_wC_biaUgl6-s1g0LlV-ClojsW8M4b1aObTffuilVeyfN0mJNlp-kL-qfSEwIVYLQnr3vYo9o2hQe3iCqYCg3KQ'
kubectl config set-context aws-hdfc-beta-mumbai --cluster=aws-hdfc-beta-mumbai --user=ChUxMTI5MDc0NDI3NjIzMTgzNjE1MDESBG9pZGM@aws-hdfc-beta-mumbai
kubectl config use-context aws-hdfc-beta-mumbai
rm ca-aws-hdfc-beta-mumbai.pem

                    """
                }
            }
        }
        stage('job file') {
            steps {
                container('helm') {
                    sh"""#!/bin/bash
                    ls
                    sed -i 's/variable/${experiment}/g' chaos/templates/job.yaml
                    sed -i 's/variable/${experiment}/g' FlockNotification.py
                    sed -i 's/variable/${experiment}/g' FlockNotification-fail.py
                    """
                }
            }
        }
        stage('Run experiment') {
            steps {
                container('helm') {
                    sh"""#!/bin/bash
                    python3 -m venv env
                    source env/bin/activate
                    pip3 install -r requirements.txt
                    ./script-loop.sh
                    """
                }
            }
        }
        stage('Install AWS') {
                    steps {
                        container('helm') {
                            sh"""#!/bin/bash
                                                        mkdir -p ./.aws
                                                        aws configure set aws_access_key_id "AKIA2HMDVXV2V3CTQLUQ"
                                                        aws configure set aws_secret_access_key "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"
                                                        aws configure set region "ap-south-1"
                                                        aws configure set output "json"
                                                        mkdir -p ./chaos-reports
                                                        aws s3 cp s3://chaos-testbucket  ./chaos-reports --recursive
                                                        ls ./chaos-reports
                            """
                        }
                    }
                }
    }
}