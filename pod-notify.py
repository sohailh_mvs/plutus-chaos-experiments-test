import glob
import json
import requests
import sys


class FlockNotification:
    failed=0
    error=0
    total=0
    data = {}
    @staticmethod
    def send_notification():

        total = 0

        flockNotificationRequestBody = f"{'<flockml>   <strong>Service is down</strong>  </flockml>'}"
        FlockNotification.data['flockml'] = flockNotificationRequestBody


        if total == 0:
            url = 'https://api.flock.com/hooks/sendMessage/acc292e5-d1c8-445c-aae7-086614019957'
            requests.post(url, headers={"Content-Type": "application/json"}, data=json.dumps(FlockNotification.data))


FlockNotificationObj = FlockNotification()
FlockNotificationObj.send_notification()
