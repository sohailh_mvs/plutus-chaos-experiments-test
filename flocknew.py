import glob
import json


class FlockNotification:

    @staticmethod
    def send_notification():
        chaos_results = glob.glob("count-pods/*.json")

        report_url = 'https://zeta-jenkins-reports.s3.ap-south-1.amazonaws.com/chaos/reportdump/28-04-2022/13/count-pods/'
        total = report_object["status"]["total"]

        if "failed" in report_object["summary"]:
            FlockNotification.failed = report_object["summary"]["failed"]

        report = {}

        for chaos_result in chaos_results:
            with open(chaos_result, 'r') as result:
                result_object = json.load(result)

            status = result_object["status"]
            experiment = result_object["experiment"]["method"][0]["provider"]["func"]
            service = result_object["experiment"]["method"][0]["provider"]["arguments"]["label_selector"].split("=")[1]

            if status == "completed":
                status = "passed"

            key = experiment + "_" + status

            if key in report:
                result = report[key]
                result.append(service)
            else:
                result = [service]

            report[key] = result

        print(report)


    flockNotificationRequestBody = f"{'<flockml><a href='}{report_url}{'>Report</a>'}{'|<br/><b>Total Services : </b>'}{str(total)}{'<br/> <b>Failed : </b>'}{FlockNotification.failed}{'<br/>'}{'<br/>'}{report}{'</flockml>'}"
    FlockNotification.data['flockml'] = flockNotificationRequestBody

url = 'https://api.flock.com/hooks/sendMessage/acc292e5-d1c8-445c-aae7-086614019957'
requests.post(url, headers={"Content-Type": "application/json"}, data=json.dumps(FlockNotification.data))

FlockNotificationObj = FlockNotification()
FlockNotificationObj.send_notification()