#!/bin/bash
for n in $(cat services-list.txt )
do
    helm uninstall -n plutus-perf $n ./chaos
    echo "-----------------------Install helm-"$n"---------------------------------------"
    helm install -n plutus-perf $n ./chaos
    kubectl -n plutus-perf get pods --selector app=plutus-perf-chaos-test-$n
done

function uninstall_helm
{
        echo "Experiment Status:" | kubectl -n plutus-perf describe pod --selector app=plutus-perf-chaos-test-$n | grep -i status:
        echo ""
        echo "----------------------------Experiment log-"$n"----------------------------"
        kubectl -n plutus-perf logs --selector app=plutus-perf-chaos-test-$n -c chaos --tail -1
        kubectl -n plutus-perf logs --selector app=plutus-perf-chaos-test-$n -c zeta --tail -1
        echo ""
        echo "----------------------------Uninstalling Helm-"$n"----------------------------"
        helm uninstall -n plutus-perf $n ./chaos 
        echo "----------------------------Helm Uninstalled-"$n"----------------------------"
        echo ""
}

for n in $(cat services-list.txt )
do
   echo ""
   echo ""
   echo "----------------------------Running Experiment-"$n"----------------------------"
   while :
   do
     kubectl -n plutus-perf wait --for=condition=complete job/plutus-perf-chaos-test-$n --timeout=5s
     if [ $? -eq 0 ]
     then
        echo $?
        uninstall_helm
        break
     fi
     kubectl -n plutus-perf wait --for=condition=failed job/plutus-perf-chaos-test-$n --timeout=5s
     if [ $? -eq 0 ] 
     then
        uninstall_helm
        break 
     fi
   done
done

# kubectl -n plutus-perf cp plutus-perf-chaos-test-$n:/tmp /home/jenkins/agent/workspace/chaos_engg_experiment -c chaostoolkit
